express = require('express');
bodyParser = require('body-parser');
mongoClient = require('mongodb').MongoClient;
bodyParser = require('body-parser');
router = express.Router();
require('dotenv').config()

router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());

var questionEndPoint  =  process.env.DB_CONN_QUESTION
var questionDBName  = process.env.DB_NAME_QUESTION
var questionCollectionName  = process.env.DB_COLLECTION_QUESTION
var questionClient = null;
var questionDB = null;
var questionCollection = null;


var userEndPoint  = process.env.DB_CONN_USER
var userDBName  = process.env.DB_NAME_USER
var userCollectionName  = process.env.DB_COLLECTION_USER
var userClient = null
var userDB = null
var userCollection = null


var STATUS_OK = "OK"
var STATUS_ERROR = "error"

mongoClient.connect(questionEndPoint, function(error, client){
  console.log("Question DB Connection Error:")
  console.log(error);
  questionClient = client;
  questionDB = client.db(questionDBName);
  questionCollection = questionDB.collection(questionCollectionName)
})

mongoClient.connect(questionEndPoint, function(error, client){
  console.log("User DB Connection Error:")
  console.log(error);
  userClient = client;
  userDB = client.db(userDBName);
  userCollection = userDB.collection(userCollectionName);
});


//var elastic = require('./db/elastic-connect.js')

router.post('/searchq', async function(req, res){
  var body = req.body;
  var timestamp = body.timestamp? body.timestamp: Math.floor(new Date()/1000)
  var limit = body.limit? body.limit: 25
  if(limit > 100)
	 limit = 100
  var accepted = body.accepted? body.accepted: false
  var q = body.q

  var sort_by = body.sort_by=="timestamp"? {"timestamp":-1}:{"score":-1}
  var has_media = body.has_media? body.has_media: false // need to have media?
  var tags = body.tags

  // format query
  var search = {}

  search["from"] = 0
  search["size"] = limit
  search['query'] = {}
  search['query']['bool'] = {}
  search['query']['bool']['must'] = []
  search['query']['bool']['filter'] = []
  search['query']['bool']['filter'].push({ "range": { "timestamp": {"lte": timestamp }} })

  if(body.sort_by=="timestamp")
    search['sort'] = { "timestamp" : {'order': 'desc', 'unmapped_type':'long'} }
  else
    search['sort'] = { "score" : {'order': 'desc', 'unmapped_type':'long'} }

  if(accepted){
    search['query']['bool']['filter'].push( { "exists": { 'field': 'accepted_answer_id' }} )
  }
  if(has_media)
    search['query']['bool']['filter'].push( { "exists": { 'field': 'media' } } )

  if(q && q.trim()!=="")
    search['query']['bool']['must'].push({
      "simple_query_string": {
          query: q,
          fields: ["title", "body"],
          default_operator: "or"
        }
      })
  else
    search['query']['bool']['must'].push( { 'match_all': {} })

  if(tags && tags.length>0)
    search['query']['bool']['filter'].push({
      "terms_set": {
        "tags": {
          "terms": tags,
          "minimum_should_match_script": {
            "source": "params.num_terms"
          }
        }
      }
    })

  var dirtyResult = await elastic.search({
    index:'stackoverflow',
    type:'questions',
    body: search
  })
  result = dirtyResult.hits.hits.map(hit => hit._source)


  var idListQuery = [];
  //var userQuestionMap = {}
  var questionUserMap = {}
  var qidQuestionMap = {}
  var returnQuestionList = [];
  for(var key in result){
    var question = result[key]
    questionUserMap[question.id] = question.username
    qidQuestionMap[question.id] = question
    returnQuestionList.push(question)             // for return later
    idListQuery.push(question.username)
  }

  getAllUsers(idListQuery, function(error, users){
    for(var key in questionUserMap){
      var username = questionUserMap[key]
      qidQuestionMap[key].user = users[username]
      delete qidQuestionMap[key].username;
    }
    res.json(decorateRes({questions: returnQuestionList}, STATUS_OK));
  })
})






router.post('/search', function(req, res){
  var body = req.body;
  var timestamp = body.timestamp? body.timestamp: Math.floor(new Date()/1000)
  var limit = body.limit? body.limit: 25
  if(limit > 100)
	 limit = 100
  var accepted = body.accepted? body.accepted: false
  var q = body.q

  var sort_by = body.sort_by=="timestamp"? {"timestamp":-1}:{"score":-1}
  var has_media = body.has_media? body.has_media: false // need to have media?
  var tags = body.tags

  var projections = {
    id: 1,
    title: 1,
    username: 1,
    body: 1,
    score: 1,
    view_count: 1,
    answer_count: 1,
    timestamp: 1,
    media: 1,
    tags: 1,
    accepted_answer_id:  1
  }

  // format query
  var questionQuery = {
    timestamp: {"$lte": timestamp}
  }
  if(accepted)
    questionQuery.accepted_answer_id = {"$ne":null}
  if(q && q.trim()!=="")
    questionQuery["$text"] = {"$search" : q}
  if(has_media)
    questionQuery.media = {"$ne": []}
  if(tags && tags.length>0)
    questionQuery.tags = {"$all": tags}

  // fire query
  questionCollection.find(questionQuery,{projection: projections}).limit(limit).sort(sort_by)
  .toArray(function(error, result){

      if(error){
        res.status(400).json(decorateRes({},STATUS_ERROR, error))
      }
      else{
        var idListQuery = [];
        //var userQuestionMap = {}
        var questionUserMap = {}
        var qidQuestionMap = {}
        var returnQuestionList = [];
        for(var key in result){
          var question = result[key]
          questionUserMap[question.id] = question.username
          qidQuestionMap[question.id] = question
          returnQuestionList.push(question)             // for return later
          idListQuery.push(question.username)
        }

        getAllUsers(idListQuery, function(error, users){
          for(var key in questionUserMap){
            var username = questionUserMap[key]
            qidQuestionMap[key].user = users[username]
            delete qidQuestionMap[key].username;
          }
          res.json(decorateRes({questions: returnQuestionList}, STATUS_OK));
        })
      }
  });

});


// get user from request
// callback(user)
function getAllUsers(idListQuery, callback){
  userCollection.find(
    {username: { "$in": idListQuery }},
    { projection:{ username:1, reputation:1 } }
  ).toArray(
      function(error, users){
        var wrappedUserList = {}
        for(var key in users){
          var user = users[key]
          var userWrap = {
            username: user.username,
            reputation: user.reputation
          }
          wrappedUserList[user.username] = userWrap;
        }
        callback(error, wrappedUserList);
      });
}


function decorateRes(resJson, status, errorMsg){
  resJson['status'] = status;
  if(errorMsg)
    resJson['error'] = errorMsg;
  return resJson;
}

module.exports = router
