express = require('express')
bodyParser = require('body-parser');
sessionUtil = require('./session-util.js')
router = express.Router()

router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());

router.use(express.static(__dirname+'/ui/public'))


validateSession = sessionUtil.validator.local();

function getUsername(req, res, next){
  req.username = sessionUtil.getSessionUsername(req)
  next()
}

router.get('/', validateSession, getUsername, function(req, res){
  res.render('search.ejs', {username: req.username})
})

router.get('/ui/login', validateSession, getUsername, function(req, res){
  res.render('login.ejs', {username: req.username})
})

router.get('/ui/register', validateSession, getUsername, function(req, res){
  res.render('register.ejs', {username: req.username})
})

router.get('/ui/verify', validateSession, getUsername, function(req, res){
  res.render('verify.ejs', {username: req.username})
})

router.get('/ui/user/:username', validateSession, getUsername, function(req, res){
  var profileUsername = req.params['username']
  res.render('user.ejs', {username: req.username, profileUsername: profileUsername})
})


router.get('/ui/questions/add', validateSession, getUsername, function(req, res){
  res.render('question_add.ejs', {username: req.username})
})

router.get('/ui/questions/:id', validateSession, getUsername, function(req, res){
  var questionID = req.params['id']
  res.render('post.ejs', {username: req.username, questionID: questionID})
})

module.exports = router;
