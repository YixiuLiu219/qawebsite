express = require('express')
uuid = require('uuid/v4');
mongoClient = require('mongodb').MongoClient;
bodyParser = require('body-parser')
require('dotenv').config()
router = express.Router()

/*
sessionUtil = require("./session-util")
send = require('gmail-send')({
	user: "yixliu@cs.stonybrook.edu",
	pass: "Sbcs110602460"
})
*/

var transporter = require('nodemailer').createTransport({
    host: 'spear.cse356.compas.cs.stonybrook.edu',
    port: 25,
    tls: { rejectUnauthorized: false },
    pool: true,
    secure: false
});




var userEndPoint  = process.env.DB_CONN_USER
var userDBName  = process.env.DB_NAME_USER
var userCollectionName  = process.env.DB_COLLECTION_USER
var userClient = null
var userDB = null
var userCollection = null

var STATUS_OK = "OK"
var STATUS_ERROR = "error"
var BACKDOOR = "abracadabra"


userDBModule = require('./db/db-user')
Users = null

mongoClient.connect(userEndPoint, function(error, client){
  console.log("User DB Connection Error:")
  console.log(error);
  userClient = client;
  userDB = client.db(userDBName);
  userCollection = userDB.collection(userCollectionName);

	Users = userDBModule.setCollection(userCollection)
});

router.post("/adduser", async function(req, res){
	var body = req.body
	if(body.username==null || body.email==null || body.password==null){
		res.status(400).json(decorateRes({}, STATUS_ERROR, "missing parameter, cannot create user"))
    return
  }

	var key = uuid()
	try{
		await Users.User().addNew(body.username, body.email, body.password, false, key, 1)
		//send email
		//
		/*
		send({
			to: body.email,
			subject: "Activation Email",
			text: "<"+key+">"
		})//
 		*/
		// respond
    if(!body.debugEmailFlag){
  		transporter.sendMail({
  			from: 'pixie@spear.cse356.compas.cs.stonybrook.edu',
  			 	to: body.email,
  			subject: 'Activation Email',
  			text: '<' + key + '>'},
  		(error, info) => {
  			if (error)
  				console.log('Mail Failed Message %s sent: %s', info.messageId, info.response);
  		});
    }
    res.json(decorateRes({}, STATUS_OK))
  }catch(error){
    res.status(400).json(decorateRes({}, STATUS_ERROR, "Create user failed, duplicate user"))
  }
})

router.post("/login", async function(req, res){
	var username = req.body.username
	var password = req.body.password

	var user = Users.User(username)
	var userData = await user.retrieve()

	if(!userData || userData.password != password){
		res.status(400).json(decorateRes({}, STATUS_ERROR, "Either username or password is incorrect"))
	}
	else if (!userData.enabled){
		res.status(400).json(decorateRes({}, STATUS_ERROR, "User not activated"))
	}
	else {
		sessionUtil.saveSessionUsername(req, userData.username)
		res.json(decorateRes({}, STATUS_OK))
	}
})


router.post("/verify", async function(req, res){
	var key = req.body.key
	var email = req.body.email
	var user = Users.UserEmail(email)
	var userData = await user.retrieve()
	if(!userData){
		res.status(400).json(decorateRes({}, STATUS_ERROR, "Invalid user email"))
	}
	else if(userData.key!==key && BACKDOOR!==key){
		res.status(400).json(decorateRes({}, STATUS_ERROR, "Wrong key"))
	}
	else{
		await user.enable()
		res.json(decorateRes({}, STATUS_OK))
	}
})

router.post("/logout", function(req, res){
	if(req.session.user){
		req.session.destroy()
		req.session = null;
		res.json(decorateRes({}, STATUS_OK))
	}
	else{
     		res.status(400).json(decorateRes({}, STATUS_ERROR, "Not logged in"))
	}
})

router.get("/sessionauth", sessionUtil.sessionAuthEndPointFunction)

function decorateRes(resJson, status, errorMsg){
  resJson['status'] = status;
  if(errorMsg)
    resJson['error'] = errorMsg;
  return resJson;
}

module.exports = router
