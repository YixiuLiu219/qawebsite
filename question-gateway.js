express = require('express')
bodyParser = require('body-parser');
proxy = require('./proxy.js');
require('dotenv').config()
router = express.Router()

var destination = process.env.API_QUESTION_MANAGE
var sessionValidateEndpoint  = process.env.ENDPOINT_SESSION_VALIDATION

var STATUS_OK = "OK"
var STATUS_ERROR = "error"

router.get("/aaaa/questions/:id", function(req, res){
  proxy.proxy(req, res, destination+"/api/questions/"+req.params['id'])
})

router.post("/questions/add", function(req, res){
  proxy.proxy(req, res, destination+"/api/questions/add")
})


router.get("/questions/:id/answers", function(req, res){
  proxy.proxy(req, res, destination+"/api/questions/"+req.params['id']+"/answers")
})

router.post("/questions/:id/answers/add", requireSession, function(req, res){
  if(req.session!=null){
    req.body.session = req.session;
    proxy.proxy(req, res, destination+"/api/questions/"+req.params['id']+"/answers/add")
  }
  else{
    res.json(decorateRes({}, STATUS_ERROR, "invalid session"))
  }
})

router.delete("/questions/:id", requireSession, function(req, res){
  if(req.session!=null){
    req.body.session = req.session;
    proxy.proxy(req, res, destination+"/api/questions/"+req.params['id'])
  }
  else{
    res.status(403)
    res.send()
  }
})


function requireSession(req, res, next){
  validateSession(req,
    function success(result){
      req.session = result.session
      next()
    },
    function fail(result){
      next()
    })
}


function validateSession(req, callback, failCallback){
  proxy.forward(req, sessionValidateEndpoint, function(error, cres, cbody){
    if(error){
      failCallback({error:error})
      return;
    }

    var jsonBody = JSON.parse(cbody);
    console.log("Auth status: "+jsonBody.status);

    if(jsonBody.status == STATUS_OK){
      callback(jsonBody);
    }
    else{
      failCallback(jsonBody);
    }
  }, "POST")
}

function decorateRes(resJson, status, errorMsg){
  resJson['status'] = status;
  if(errorMsg)
    resJson['error'] = errorMsg;
  return resJson;
}

module.exports = router
