express = require('express')
mongoClient = require('mongodb').MongoClient;
questionDBModule = require('./db/db-question')
userDBModule = require('./db/db-user')
answerDBModule = require('./db/db-answer')

require('dotenv').config()

router = express.Router()

var sessionValidateEndpoint  = process.env.ENDPOINT_SESSION_VALIDATION

var questionEndPoint  = process.env.DB_CONN_QUESTION
var questionDBName  = process.env.DB_NAME_QUESTION
var questionCollectionName  = process.env.DB_COLLECTION_QUESTION
var answerCollectionName = process.env.DB_COLLECTION_ANSWER
var questionClient = null;
var questionDB = null;
var questionCollection = null;
var answerCollection = null;

var userEndPoint  = process.env.DB_CONN_USER
var userDBName  = process.env.DB_NAME_USER
var userCollectionName  = process.env.DB_COLLECTION_USER
var userClient = null
var userDB = null
var userCollection = null


var STATUS_OK = "OK"
var STATUS_ERROR = "error"


Questions = null
Answers = null
Users = null


mongoClient.connect(userEndPoint, function(error, client){
  console.log("User DB Connection Error:")
  console.log(error);
  userClient = client;
  userDB = client.db(userDBName);
  userCollection = userDB.collection(userCollectionName);

  Users = userDBModule.setCollection(userCollection)
})

mongoClient.connect(questionEndPoint, function(error, client){
  console.log("Question DB Connection Error:")
  console.log(error);
  questionClient = client;
  questionDB = client.db(questionDBName);
  questionCollection = questionDB.collection(questionCollectionName);
  answerCollection = questionDB.collection(answerCollectionName);

  Questions = questionDBModule.setCollection(questionCollection)
  Answers = answerDBModule.setCollection(answerCollection)
})




router.get('/user/:username', async function(req, res){
  var projections = {
    _id: 0,
    email: 1,
    reputation: 1
  }
  var user = await Users.User(req.params['username']).retrieve(projections)
  if(!user){
    res.status(400).json(decorateRes({}, STATUS_ERROR, "Invalid User"))
  }
  else{
    res.json(decorateRes({ user: user }, STATUS_OK))
  }

})


router.get("/user/:username/questions", requireUser, async function(req, res){
  var username = req.username
  var projections = { id: 1 }
  var questions = await Questions.Question().retrieveAllByUser(username, projections)

  var questionIDList = []
  for(var key in questions){
    var questionID = questions[key].id
    questionIDList.push(questionID)
  }
  res.json(decorateRes({ questions: questionIDList }, STATUS_OK))

})


router.get("/user/:username/answers", requireUser, async function(req, res){
  var username = req.username;
  var projections = { _id: 0, "id": 1 }

  var answers = await Answers.Answer().retrieveAllByUser(username, projections)
  var answerIDList = []
  for(var key in answers){
    var answer = answers[key].id
    answerIDList.push(answer)
  }
  res.json(decorateRes({ answers:answerIDList }, STATUS_OK))

})

async function requireUser(req, res, next){
  // find user
  var userData = await Users.User(req.params["username"]).retrieve()
  if(!userData){
   res.status(400).json(decorateRes({}, STATUS_ERROR, "Invalid User"))
 }
 // continue to query question db
 else{
   req.username = userData.username
   next()
 }

}

function decorateRes(resJson, status, errorMsg){
  resJson['status'] = status;
  if(errorMsg)
    resJson['error'] = errorMsg;
  return resJson;
}

module.exports = router;
