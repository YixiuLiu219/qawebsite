
mongoClient = require('mongodb').MongoClient;
require('dotenv').config()

var urlConnectionMap = {}
var nameCollectionMap = {}

/*
var questionName = process.env.DB_COLLECTION_QUESTION
var answerName = process.env.DB_COLLECTION_ANSWER
var upvoteName = process.env.DB_COLLECTION_UPVOTE
var viewName = process.env.DB_COLLECTION_VIEW_HISTORY
var userName = process.env.DB_COLLECTION_USER
var questionCollection = connect(process.env.DB_CONN_QUESTION, process.env.DB_NAME_QUESTION, questionName)
var answerCollection = connect(process.env.DB_CONN_QUESTION, process.env.DB_NAME_QUESTION, answerName)
var upvoteCollection = connect(process.env.DB_CONN_QUESTION, process.env.DB_NAME_QUESTION, upvoteName)
var viewCollection = connect(process.env.DB_CONN_QUESTION, process.env.DB_NAME_QUESTION, viewName)
var userCollection = connect(process.env.DB_CONN_USER, process.env.DB_NAME_USER, userName)
*/

function connect(url, dbName, collectionName){
  var conn = urlConnectionMap[url]
  if(!conn){ // hasn't been initialized
    var conn = await mongoClient.connect(url)
    if(!conn){
      console.log("error connecting to DB: "+dbName+" "+url)
    }
    else{
      urlConnectionMap[url] = conn
    }
    console.log("New Connection To: "+dbName)
  }
  nameCollectionMap[collectionName] = urlConnectionMap[url].db(dbName).collection(collectionName)
  return nameCollectionMap[collectionName]
}

function getCollection(collectionName){
  return nameCollectionMap[collectionName]
}

var questionCollection = null
function connectQuestion(url, dbName, collectionName){
  questionCollection = connect(url, dbName, collectionName)
}
var answerCollection = null
function connectAnswer(url, dbName, collectionName){
  answerCollection = connect(url, dbName, collectionName)
}
var upvoteCollection = null
function connectUpvote(url, dbName, collectionName){
  upvoteCollection = connect(url, dbName, collectionName)
}
var viewCollection = null
function connectView(url, dbName, collectionName){
  viewCollection = connect(url, dbName, collectionName)
}
var userCollection = null
function connectUser(url, dbName, collectionName){
  userCollection = connect(url, dbName, collectionName)
}

var questionProjection = {
  id: 1,
  title: 1,
  username: 1,
  body: 1,
  score: 1,
  view_count: 1,
  answer_count: 1,
  timestamp: 1,
  media: 1,
  tags: 1,
  accepted_answer_id:  1
}


function Model(fields, collection){
  return {
    collection: collection,
    fields: fields,
    get: get,
    update: update,
    upsert: upsert,
    inc: inc,
    insert: insert
  }
}

function Answer(fields){
  return Model(fields, answerCollection)
}
function Question(fields){
  return Model(fields, questionCollection)
}
function User(fields){
  return Model(fields, userCollection)
}
function View(fields){
  return Model(fields, viewCollection)
}
function Upvote(fields){
  return Model(fields, upvoteCollection)
}

// all
function get(){
  return this.collection.findOne(this.fields)
}

function update(newContent){
  return this.collection.updateOne(this.fields, {"$set": newContent})
}

function inc(newContent){
  return this.collection.updateOne(this.fields, {"$inc": newContent})
}

function upsert(newContent){
  return this.collection.updateOne(this.fields, {"$set": newContent}, {"upsert" : true})
}

function insert(content){
  return this.collection.insertOne(content)
}

// quetion + answer
function incScore(amt){
  return this.inc({"score" : amt}})
}

// question
function accept(answerID){
  return this.update({'accepted_answer_id': answerID})
}

function incView(){
  return this.inc({"view_count":1}})
}

// answer
function markAccepted(){
  return this.update({'is_accepted': true})
}


// user
function incRep(amt){
  return this.inc({"reputation" : amt})
}


module.exports = {

}
