//user management

express = require('express');
request = require('request');
proxy = require('./proxy.js');
require('dotenv').config()


router = express.Router();

var userEndPoint = process.env.API_USER_VALIDATION //'http://45.32.212.106:8080' //'http://130.245.168.55:8080'
var userStatEndPoint = process.env.API_USER_STAT

/*
router.post('/adduser', function(req, res){
  var url = userEndPoint+'/adduser'
	proxy.proxy(req, res, url);
});

router.post('/verify', function(req, res){
  var url = userEndPoint+'/verify'
	proxy.proxy(req, res, url);
});

//test = "adb1f7ee-3b40-4e9f-954f-9242d1895f26";

router.post('/login', function(req, res){
  var url = userEndPoint+'/login'
  //var url = 'http://localhost:8080/logout'
  proxy.proxy(req, res, url);
});

router.post('/logout', function(req, res){
  var url = userEndPoint+'/logout'
  proxy.proxy(req, res, url);
});
*/

router.get('/user/:username', function(req, res){
  var url = userStatEndPoint+'/api/user/'+req.params['username']
  proxy.proxy(req, res, url);
});
router.get('/user/:username/questions', function(req, res){
  var url = userStatEndPoint+'/api/user/'+req.params['username']+'/questions'
  proxy.proxy(req, res, url);
});
router.get('/user/:username/answers', function(req, res){
  var url = userStatEndPoint+'/api/user/'+req.params['username']+'/answers'
  proxy.proxy(req, res, url);
});

module.exports = router;
