

require('dotenv').config()
request = require('request-promise')

const elasticQuestionURL = process.env.DB_CONN_ELASTIC + "/" + process.env.ELASTIC_QUESTION

var elastic = {
  addNew: addNew
}

function addNew(q){
  return request({
    uri: elasticQuestionURL,
    method: "post",
    body: q,
    json: true,
  })
}

module.exports = elastic
