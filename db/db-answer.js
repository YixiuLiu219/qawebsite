
function DBAnswer(id, collection){
  var answer = {
    collection: collection,
    data: { id: id },

    retrieve: retrieve,
    retrieveAll: retrieveAll,
    incScore: incScore,
    accept: accept,
    remove: remove,
    removeAll: removeAll,
    retrieveAllByUser: retrieveAllByUser,
    retrieveAllByQuestion: retrieveAllByQuestion,
    removeAllByQuestion: removeAllByQuestion,
    addNew: addNew
  }

  return answer
}

function retrieve(projection){
  if(projection)
    return this.collection.findOne(this.data, {projection: projection})
  else
    return this.collection.findOne(this.data)
}

function retrieveAll(aidList, projection){
  if(projection)
    return this.collection.find({ id: { "$in": aidList } }, { projection: projection }).toArray()
  else
    return this.collection.find({ id: { "$in": aidList } }).toArray()
}

function removeAll(aidList){
  return this.collection.deleteMany({ id: { "$in": aidList } })
}

function incScore(amt){
  return this.collection.updateOne(this.data, { "$inc": {"score" : amt} } )
}

function remove(){
  return this.collection.deleteOne(this.data)
}

function accept(){
  return this.collection.updateOne(this.data, {"$set": {'is_accepted': true} })
}

function removeAllByQuestion(qid){
    return this.collection.deleteMany({ question_id: qid })
}

function retrieveAllByQuestion(qid, projection){
  if(projection)
    return this.collection.find({ question_id: qid }, { projection: projection }).toArray()
  else
    return this.collection.find({ question_id: qid }).toArray()
}

function retrieveAllByUser(username, projection){
  if(projection)
    return this.collection.find({ username: username }, { projection: projection }).toArray()
  else
    return this.collection.find({ username: username }).toArray()
}

function addNew(aid, qid, username, body, timestamp, mediaIDs){
  return this.collection.insertOne({
		id: aid,
    question_id: qid,
		username: username,
		body: body,
		timestamp: timestamp,
		score: 0,
		media: mediaIDs,
		is_accepted: false
	})
}

module.exports = {
  setCollection: function(collection){
    return {
      Answer: function(id){
        return DBAnswer(id, collection)
      }
    }
  }
}
