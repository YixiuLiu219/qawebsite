
function DBUpvote(username, post_id, post_type, collection){
  var upvote = {
    collection: collection,
    data: { username: username, post_id: post_id, post_type: post_type },

    retrieve: retrieve,
    upsert: upsert
  }

  return upvote
}

function retrieve(projection){
  if(projection)
    return this.collection.findOne(this.data, {projection: projection})
  else
    return this.collection.findOne(this.data)
}

function upsert(username, post_id, post_type, action, waived){
  return this.collection.updateOne(
    { username: username, post_id: post_id, post_type: post_type },
    { "$set": { action: action, waived: waived } },
    { "upsert": true })
}

module.exports = {
  setCollection: function(collection){
    return {
      Upvote: function(username, post_id, post_type){
        return DBUpvote(username, post_id, post_type, collection)
      }
    }
  }
}
