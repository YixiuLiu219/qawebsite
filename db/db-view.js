
function DBView(id, user, collection){
  var view = {
    collection: collection,
    data: { id: id, user: user },

    retrieve: retrieve,
    addNew: addNew
  }

  return view
}

function retrieve(projection){
  if(projection)
    return this.collection.findOne(this.data, {projection: projection})
  else
    return this.collection.findOne(this.data)
}

function addNew(){
  return this.collection.insertOne(this.data)
}

module.exports = {
  setCollection: function(collection){
    return {
      View: function(id, user){
        return DBView(id, user, collection)
      }
    }
  }
}
