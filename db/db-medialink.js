
function DBMediaLink(ids, username, collection){
  var mediaLink = {
    collection: collection,
    data: { ids: ids , username: username },

    retrieveValid: retrieveValid,
    addNew: addNew,
    claim: claim
  }

  return mediaLink
}

function retrieveValid(projection){
  var username = this.data.username;
  var ids = this.data.ids
  if(projection)
    return this.collection.find({id: { "$in" : ids }, username: username, claimed:false }, {projection: projection}).toArray()
  else
    return this.collection.find({id: { "$in" : ids }, username: username, claimed:false }).toArray()
}

function addNew(){
  return this.collection.insertOne({    // add 1 media at a time
    id: this.data.ids,
    username: this.data.username,
    claimed: false
  })
}

function claim(){
  return this.collection.updateMany({id: { "$in" : this.data.ids }}, {"$set": {'claimed': true} })
}

module.exports = {
  setCollection: function(collection){
    return {
      MediaLink: function(ids, username){
        return DBMediaLink(ids, username, collection)
      }
    }
  }
}
