
function DBQuestion(id, collection){
  var question = {
    collection: collection,
    data: { id: id },

    retrieve: retrieve,
    incView: incView,
    incScore: incScore,
    accept: accept,
    incAnswerAndRetrieve: incAnswerAndRetrieve,
    remove: remove,
    retrieveAllByUser: retrieveAllByUser,
    addNew: addNew,
    insertAnswer: insertAnswer,

    //incElasticAnswer: function(id){ return elasticIncAnswer(id) }
  }

  return question
}

function retrieve(projection){
  if(projection)
    return this.collection.findOne(this.data, {projection: projection})
  else
    return this.collection.findOne(this.data)
}

function incAnswerAndRetrieve(amt, projection){
  if(projection)
    return this.collection.findOneAndUpdate(this.data, { "$inc": { answer_count: amt } }, {projection: projection})
  else
    return this.collection.findOneAndUpdate(this.data, { "$inc": { answer_count: amt } })
}

function incView(amt){
  return this.collection.updateOne(this.data, { '$inc': { view_count: amt } } )
/*
  return Promise.all([
    this.collection.updateOne(this.data, { '$inc': { view_count: amt } } ),
    elasticIncView(this.data.id)
  ])
*/
}

function incScore(amt){
  return this.collection.updateOne(this.data, { "$inc": {"score" : amt} } )
  /*
  return Promise.all([
    this.collection.updateOne(this.data, { "$inc": {"score" : amt} } ),
    elasticIncScore(this.data.id, amt)
  ])
  */
}

function remove(){
  return this.collection.deleteOne(this.data)
/*
  return Promise.all([
    this.collection.deleteOne(this.data),
    elasticRemove(this.data.id)
  ])*/
}

function accept(answerID){
  return this.collection.updateOne(this.data, {"$set": {'accepted_answer_id': answerID} })
    /*
  return Promise.all([
    this.collection.updateOne(this.data, {"$set": {'accepted_answer_id': answerID} }),
    elasticAccept(this.data.id, answerID)
  ])
  */
}

function insertAnswer(answerID){
  return this.collection.updateOne(this.data, {"$push": {'answers': answerID} })

  /*
  return Promise.all([
    this.collection.updateOne(this.data, {"$push": {'answers': answerID} }),
    elasticInsertAnswer(this.data.id, answerID)
  ])
  */
}

function retrieveAllByUser(username, projection){
  if(projection)
    return this.collection.find({ username: username }, { projection: projection }).toArray()
  else
    return this.collection.find({ username: username }).toArray()
}

function addNew(qid, uid, title, body, tags, timestamp, mediaIDs){
  var q = {
		id: qid,
		username: uid,
    title: title,
		body: body,
		tags: tags,
		timestamp: timestamp,
		score: 0,
		view_count: 0,
		answer_count: 0,
    answers: [],
		media: mediaIDs,
		accepted_answer_id: null
	}
  return this.collection.insertOne(q)
  /*
  return Promise.all([
    this.collection.insertOne(q),
    elasticAddNew(q)
  ])
  */
}




function elasticAddNew(question){
  delete question._id
  return elastic.index({
    index: 'stackoverflow',
    id: question.id,
    type: 'questions',
    body: question
  })
}

function elasticIncAnswer(id){
  return elastic.update({
    index: 'stackoverflow',
    type: 'questions',
    id: id,
    body: {
      script: "ctx._source.answer_count++"
    }
  })
}

function elasticIncView(id){
  return elastic.update({
    index: 'stackoverflow',
    type: 'questions',
    id: id,
    body: {
      script: "ctx._source.view_count++"
    }
  })
}

function elasticIncScore(id, amt){
  return elastic.update({
    index: 'stackoverflow',
    type: 'questions',
    id: id,
    body: {
      script: {
        source: "ctx._source.score+=params.amt",
        params: {
          "amt" : amt
        }
      }
    }

  })
}

function elasticRemove(id){
  return elastic.delete({
    index: 'stackoverflow',
    type: 'questions',
    id: id
  })
}

function elasticAccept(id, answerID){
  return elastic.update({
    index: 'stackoverflow',
    type: 'questions',
    id: id,
    body:{
      doc:{
        accepted_answer_id : answerID
      }
    }
  })
}

function elasticInsertAnswer(id, answerID){///////////////
  return elastic.update({
    index: 'stackoverflow',
    type: 'questions',
    id: id,
    body:{
      script: {
        source: 'ctx._source.answers.add(params.answerID)',
        params: {
          answerID: answerID
        }
      }
    }
  })
}

//var elastic = require('./elastic.js')
//var elastic = require('./elastic-connect.js')

module.exports = {
  setCollection: function(collection){
    return {
      Question: function(id){
        return DBQuestion(id, collection)
      }
    }
  }
}
