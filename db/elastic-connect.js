
require('dotenv').config()
elasticsearch = require('elasticsearch')

var client = elasticsearch.Client({ host: process.env.DB_CONN_ELASTIC })
client.cluster.health({},function(err,resp,status) {
  console.log("-- Client Health --",resp);
})

module.exports = client
