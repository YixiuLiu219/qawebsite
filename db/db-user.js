
function DBUser(username, userCollection){
  var user = {
    collection: userCollection,
    data: { username: username },
    retrieve: retrieve,
    enable: enable,
    incRep: incRep,
    addNew: addNew
  }
  return user;
}

function DBUserEmail(email, collection){
  var user = DBUser(null, collection)
  user.data = { email: email }
  return user;
}


function addNew(username, email, password, enabled, key, reputation){
  return this.collection.insertOne({
		username: username,
		email: email,
		password: password,
		enabled: enabled,
		key: key,
		reputation: reputation
	})
}


function retrieve(projection){
  if(projection)
    return this.collection.findOne(this.data, {projection: projection})
  else
    return this.collection.findOne(this.data)
}

function enable(){
  return this.collection.updateOne(this.data, { "$set": { enabled: true } })
}

function incRep(change){
  return this.collection.updateOne(this.data, {"$inc":{"reputation" : change}})
}

module.exports = {
  setCollection: function(collection){
    return {
      User: function(username){
        return DBUser(username, collection)
      },
      UserEmail: function(email){
        return DBUserEmail(email, collection)
      }

    }
  }
}
