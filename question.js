express = require('express')
proxy = require('./proxy.js');
uuid = require('uuid/v4');
mongoClient = require('mongodb').MongoClient;
requestIP = require('request-ip');
bodyParser = require('body-parser');
require('dotenv').config()
sessionUtil = require('./session-util.js')
request = require('request')
questionDBModule = require('./db/db-question')
userDBModule = require('./db/db-user')
viewDBModule = require('./db/db-view')
answerDBModule = require('./db/db-answer')
upvoteDBModule = require('./db/db-upvote')
mediaLinkDBModule = require('./db/db-medialink')

router = express.Router()

router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());

var sessionValidateEndpoint  = process.env.ENDPOINT_SESSION_VALIDATION

var questionEndPoint  = process.env.DB_CONN_QUESTION
var questionDBName  = process.env.DB_NAME_QUESTION
var questionCollectionName  = process.env.DB_COLLECTION_QUESTION
var answerCollectionName = process.env.DB_COLLECTION_ANSWER
var questionClient = null;
var questionDB = null;
var questionCollection = null;
var answerCollection = null;
var upvoteCollectionName = process.env.DB_COLLECTION_UPVOTE
var upvoteCollection = null
var viewCollectionName = process.env.DB_COLLECTION_VIEW_HISTORY
var viewCollection = null

var userEndPoint  = process.env.DB_CONN_USER
var userDBName  = process.env.DB_NAME_USER
var userCollectionName  = process.env.DB_COLLECTION_USER
var userClient = null
var userDB = null
var userCollection = null

var mediaLinkCollectionName = process.env.DB_COLLECTION_MEDIA_LINK


var STATUS_OK = "OK"
var STATUS_ERROR = "error"

var POST_TYPE_QUESTION = 1
var POST_TYPE_ANSWER = 2


//validateSession = sessionUtil.validator.distant("http://localhost:8080/sessionauth")
validateSession = sessionUtil.validator.local();


Questions = null
Users = null
Views = null
Answers = null
Upvotes = null
MediaLink = null

// establish db connections
mongoClient.connect(questionEndPoint, function(error, client){
  console.log("Question DB Connection Error:")
  console.log(error);
  questionClient = client;
  questionDB = client.db(questionDBName);
  questionCollection = questionDB.collection(questionCollectionName);
  answerCollection = questionDB.collection(answerCollectionName);
  upvoteCollection = questionDB.collection(upvoteCollectionName);
  viewCollection = questionDB.collection(viewCollectionName);
  mediaLinkCollection = questionDB.collection(mediaLinkCollectionName);

  Questions = questionDBModule.setCollection(questionCollection)
  Answers = answerDBModule.setCollection(answerCollection)
  Views = viewDBModule.setCollection(viewCollection)
  Upvotes = upvoteDBModule.setCollection(upvoteCollection)
  MediaLink = mediaLinkDBModule.setCollection(mediaLinkCollection)
});


mongoClient.connect(userEndPoint, function(error, client){
  console.log("User DB Connection Error:")
  console.log(error);
  userClient = client;
  userDB = client.db(userDBName);
  userCollection = userDB.collection(userCollectionName);

  Users = userDBModule.setCollection(userCollection)
});


router.post('/link', async function(req, res){
  var id = req.body.id;
  var username = req.body.username;
  await MediaLink.MediaLink(id, username).addNew()
  res.json(decorateRes({}, STATUS_OK))
})

router.post('/validateAndInsert', async function(req, res){
  var ids = req.body.ids;
  var username = req.body.username;
  var mediaLink = MediaLink.MediaLink(ids, username)
  var results = await mediaLink.retrieveValid()
  if(results.length != ids.length){
    res.json({nope:"nope"})
    return
  }
  await mediaLink.claim()
  res.json({yes:"yes"})

})

router.get('/questions/:id', validateSession, async function(req, res){
  //console.log("get questions from IP:")
  //console.log(requestIP.getClientIp(req))
  var username = sessionUtil.getSessionUsername(req)
  var userIdentification = username? username: requestIP.getClientIp(req)

  var qid = req.params['id']
  var projections = {
    _id: 0,
    answers: 0
  }
  var question = Questions.Question(qid)
  var questionData = await question.retrieve(projections)
  if(!questionData){
    res.status(400).json(decorateRes({}, STATUS_ERROR, "invalid question id"))
  }
  else{
    var view = Views.View(qid, userIdentification)
    try{
      await view.addNew()
      question.incView(1)
      questionData.view_count++
    }catch(error){
      // no increase in view count
    }

    var userData = await Users.User(questionData.username).retrieve()
    var resUser = {
      username: userData.username,
      reputation: userData.reputation
    }

    delete questionData.username;
    questionData.user = resUser
    res.json(decorateRes({question: questionData}, STATUS_OK));
  }
});

router.post('/questions/add', validateSession, async function(req, res){
  if(req.body.title==null || req.body.body==null || req.body.tags==null || req.body.title.trim().length<=0 || req.body.body.trim().length<=0){
    res.status(400).json(decorateRes({}, STATUS_ERROR, "missing parameter"))
    return
  }
  var username = sessionUtil.getSessionUsername(req)
  if(username){
    // set fields
    var uid = username;
    var qid = uuid();
    var title = req.body.title
    var body = req.body.body;
    var tags = req.body.tags;
    var timestamp = Math.floor(new Date()/1000);
    var mediaIDs = req.body.media?req.body.media:[]

    var question = Questions.Question()

    // check for shared media
    var mediaLink = MediaLink.MediaLink(mediaIDs, username)
    var results = await mediaLink.retrieveValid()
    if(results.length != mediaIDs.length){
      res.status(403).json(decorateRes({}, STATUS_ERROR, "Using shared, not owned, or invalid Media IDs"))
      return
    }

    await Promise.all([
      mediaLink.claim(),
      question.addNew(qid, uid, title, body, tags, timestamp, mediaIDs)
    ])

    // return to client
    res.json(decorateRes({id: qid}, STATUS_OK));
  }
  else{
    res.status(400).json(decorateRes({}, STATUS_ERROR, "Invalid Session"))
  }
});




router.post("/questions/:id/answers/add", validateSession, async function(req, res){
  if(req.body.body==null){
    res.status(400).json(decorateRes({}, STATUS_ERROR, "missing answer body text"))
    return
  }

  var username = sessionUtil.getSessionUsername(req);
  if(!username){
    res.status(400).json(decorateRes({}, STATUS_ERROR, "invalid session"))
    return
  }

  //check question exist + increment answer count
  var question = Questions.Question(req.params['id'])
  var questionData = await question.incAnswerAndRetrieve(1, { id: 1 })    //flawed, ebcause could return below without adding answer
  if(questionData.value==null){
    res.status(400).json(decorateRes({}, STATUS_ERROR, "invalid question id"))
  }
  else{
    //await question.incElasticAnswer(req.params['id'])
    var aid = uuid()
    var qid = req.params['id']
    var body = req.body.body
    var timestamp = Math.floor(new Date()/1000)
    var mediaIDs = req.body.media?req.body.media:[]

    // check for shared media
    var mediaLink = MediaLink.MediaLink(mediaIDs, username)
    var results = await mediaLink.retrieveValid()
    if(results.length != mediaIDs.length){
      res.status(403).json(decorateRes({}, STATUS_ERROR, "Using shared, not owned, or invalid Media IDs"))
      return
    }

    await Promise.all([
      mediaLink.claim(),
      Answers.Answer().addNew(aid, qid, username, body, timestamp, mediaIDs),
      question.insertAnswer(aid)
    ])
    res.json(decorateRes({id: aid}, STATUS_OK))
  }
})

router.get("/questions/:id/answers", async function(req, res){
  var question = await Questions.Question(req.params['id'])
  var questionData = await question.retrieve({answers: 1})
//console.log(questionData)
  if(questionData==null){
    res.status(400).json(decorateRes({}, STATUS_ERROR, "invalid question id"))
  }
  else{
    var projection = { _id: 0, question_id: 0 }
    //var answers = await Answers.Answer().retrieveAllByQuestion(req.params['id'], projection)
    var answers = await Answers.Answer().retrieveAll(questionData.answers, projection)
    var returnAnswerObject = []
    for(var key in answers){
      var answerObject = answers[key]
      returnAnswerObject.push({
        id: answerObject.id,
        user: answerObject.username,
        body: answerObject.body,
        score: answerObject.score,
        is_accepted: answerObject.is_accepted,
        timestamp: answerObject.timestamp,
        media: answerObject.media,
      })
    }
    res.json(decorateRes({answers: returnAnswerObject}, STATUS_OK));
  }
})



router.delete("/questions/:id", validateSession, async function(req, res){

  var username = sessionUtil.getSessionUsername(req);
  if(!username){
    res.status(403).send()
    return;
  }

  var questionID = req.params['id']
  var projection = { username: 1, media: 1 , answers: 1}
  var mediaIDs = []
  var answerIDs = []
  // get question
  var question = Questions.Question(questionID)
  var questionData = await question.retrieve(projection)
  if(questionData==null){
    res.status(404).send();
  }
  else if(username !== questionData.username){   // Forbidden. conviently also checks for null, even though it's checked above
    res.status(403).send()
  }
  else{
    mediaIDs = mediaIDs.concat(questionData.media)
    var answerProjection = { id: 1, media: 1 }

    // get answer
    //var answers = await Answers.Answer().retrieveAllByQuestion(questionID, answerProjection)
    var answers = await Answers.Answer().retrieveAll(questionData.answers, answerProjection)
    for(var key in answers){
      var answer = answers[key]

      answerIDs.push(answer.id)
      mediaIDs = mediaIDs.concat(answer.media)
    }

    // start delete
    await Promise.all([
      question.remove(),
      //Answers.Answer().removeAllByQuestion(questionID),
      Answers.Answer().removeAll(answerIDs),
      new Promise(function(resolve){
        var headers = {}
        headers['Content-Type'] = 'application/json'
        let options = {
          body: JSON.stringify({ ids: mediaIDs }),
          method: 'POST',
          url: process.env.ENDPOINT_MEDIA_DELETE,
          headers: headers
        }
        request(options, function(error, deleteRes, deleteBody){
          if(error)
            console.log(error)
          resolve()
        })
      })

    ])
    res.status(200).send()

  }
})



router.post("/answers/:id/accept", validateSession, async function(req, res){
  var username = sessionUtil.getSessionUsername(req)
  if(!username){
    res.status(400).json(decorateRes({}, STATUS_ERROR, "invalid session"))
  }
  else{
    var aid = req.params["id"]

    var answer = Answers.Answer(aid)
    var answerData = await answer.retrieve()
    if(answerData==null){
      res.status(400).json(decorateRes({}, STATUS_ERROR, "invalid answer id"))
    }
    else if(answerData['is_accepted']){
      res.status(400).json(decorateRes({}, STATUS_ERROR, "this answer is already accepted"))
    }
    else{
      var qid = answerData['question_id']
      var question = Questions.Question(qid)
      var questionData = await question.retrieve()
      var qUsername = questionData['username']

      if(questionData['accepted_answer_id']!=null){
        res.status(400).json(decorateRes({}, STATUS_ERROR, "the question already has accepted answer"))
      }
      else if(qUsername!=username){
        res.status(400).json(decorateRes({}, STATUS_ERROR, "user is not the question poster"))
      }
      else{
        Promise.all([
          question.accept(aid),
          answer.accept(),
        ])
        .then(function(){
          res.json(decorateRes({}, STATUS_OK))
        })
      }
    }

  }

})



router.post("/questions/:id/upvote", setPostTypeQuestion, validateSession, upvote)
router.post("/answers/:id/upvote", setPostTypeAnswer, validateSession, upvote)

function setPostTypeQuestion(req, res, next){
  req.postType = POST_TYPE_QUESTION
  next()
}
function setPostTypeAnswer(req, res, next){
  req.postType = POST_TYPE_ANSWER
  next()
}
async function upvote(req, res){
  var username = sessionUtil.getSessionUsername(req)
  if(!username){
    res.status(400).json(decorateRes({}, STATUS_ERROR, "invalid session"))
  }
  else{
    var action = req.body.upvote!=null? (req.body.upvote? VOTE_UP: VOTE_DOWN): VOTE_UP
    var postType = req.postType
    var id = req.params['id']

    var qa = (postType==POST_TYPE_QUESTION)? Questions.Question(id): Answers.Answer(id)
    var upvote = Upvotes.Upvote(username, id, postType)

    var [post, vote] = await Promise.all([ qa.retrieve(), upvote.retrieve() ])
    if(!post){
      res.status(400).json(decorateRes({}, STATUS_ERROR, "Invalid post id"))
      return
    }

    var user = Users.User(post.username)
    var postUser = await user.retrieve()

    var repChange = 0
    var waived = false
    //var action = action
    var scoreChange = 0
    //var postUser = postUser
    var voterUsername = username
    var id = post.id
    var type = postType
    //console.log("VOTE DEBUG")
    //console.log(!vote)
    //console.log(action)
    if(action==VOTE_UP){  // input==upvote
      if(!vote || vote.action==VOTE_NEUTRAL){ //past neutral
        repChange = 1
        scoreChange = 1
        //console.log("CHECK VOTE DB CONTENT")
        //console.log(vote)
      }
      else if(vote.action==VOTE_DOWN){ //past down
        if(vote.waived){
          repChange = 1
        }
        else{
          repChange = 2
        }
        scoreChange = 2
      }
      else if(vote.action==VOTE_UP){ // past up, so this is undo upvote
        if(postUser.reputation-1 < 1){
          repChange = 0
          waived = true
        }
        else{
          repChange = -1
        }
        action = VOTE_NEUTRAL
        scoreChange = -1
      }
    }
    else if(action==VOTE_DOWN){ // input==downvote
      if(!vote ||  vote.action==VOTE_NEUTRAL){  // past neutral
        if(postUser.reputation - 1 < 1){
          repChange = 0
          waived = true
        }
        else{
          repChange = -1
        }
        scoreChange = -1
      }
      else if(vote.action==VOTE_UP){  // past up
        if(postUser.reputation - 2 < 1){
          repChange = -(postUser.reputation - 1)
          waived = true
        }
        else{
          repChange = -2
        }
        scoreChange = -2
      }
      else if(vote.action==VOTE_DOWN){  // undo downvote
        if(vote.waived){
          repChange = 0
        }
        else{
          repChange = 1
        }
        action = VOTE_NEUTRAL
        scoreChange = 1
      }
    }

    //update: Q/A, user rep, upvote
    await Promise.all([
      qa.incScore(scoreChange),
      user.incRep(repChange),
      upvote.upsert(voterUsername, post.id, postType, action, waived)
    ])
    res.json(decorateRes({}, STATUS_OK))


  }
}

const VOTE_NEUTRAL = 2
const VOTE_DOWN = 0
const VOTE_UP = 1


// get user from request
// callback(user)
function getUser(userID, callback){
  userCollection.findOne({ username: userID }, function(error, user){
    var userWrap = {
      //id: user.username,
      username: user.username,
      reputation: user.reputation
    }
    callback(userWrap);
  });
}


function decorateRes(resJson, status, errorMsg){
  resJson['status'] = status;
  if(errorMsg)
    resJson['error'] = errorMsg;
  return resJson;
}

function createQuestion(qid, uid, title, body, tags, timestamp, mediaIDs){
  var question = {
		id: qid,
		username: uid,
    title: title,
		body: body,
		tags: tags,
		timestamp: timestamp,
		score: 0,
		view_count: 0,
		answer_count: 0,
    answers: [],
		media: mediaIDs,
		accepted_answer_id: null
	}
  return question;
}

function createAnswer(aid, qid, uid, body, timestamp, mediaIDs){
  var answer = {
		id: aid,
    question_id: qid,
		username: uid,
		body: body,
		timestamp: timestamp,
		score: 0,
		media: mediaIDs,
		is_accepted: false
	}
  return answer;
}

module.exports = router
