express = require('express');
//userGate = require('./user-gateway.js')
//questionGate = require('./question-gateway.js')
questionAPI = require('./question.js')
searchAPI = require('./search.js')
pageAPI = require('./page.js')
userStatAPI = require('./userstat.js')
proxy = require('./proxy.js')
userAPI = require('./user.js')
bodyParser = require('body-parser')
sessionUtil = require('./session-util')
path = require('path')

app = express();
app.set('views', path.join(__dirname, '/ui/views'));
app.set('view engine', 'ejs')
app.use(sessionUtil.sessionMiddleware())
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));

logger = function(req, res, next){
    console.log("Incoming Request...")
    console.log(req.method+" "+req.url)
    console.log(req.body)
    next();
}

//app.use(logger);

//app.use(userGate);
//app.use(questionGate);

// these routers can be deployed to another server
app.use(questionAPI);
app.use(searchAPI);
app.use(userStatAPI);
app.use(userAPI);
app.use(pageAPI); // IMPORTANT TO PUT LAST!!

port = 8080;



app.get("/test", function(req, res){
   res.send("hello world");
});



app.listen(port, function(){
  console.log("listening on port: "+port);
});
