
$(document).ready(function(){
  $("#logoutBtn").on('click', function(){ logout(redirectToMain); } )
})

function logout(success){
  $.ajax({
    url: '/logout',
    method: 'POST',
    contentType: "application/json",
    success: success
  })
}

function redirectToMain(data, status){
  window.location = "/"
}
