

$(document).ready(function(){
  $("#register").on('click', function(){ register(redirectToVerify, clearFieldsAndDisplayError); } )
})

function register(success, error){
  var username = $('#username').val()
  var password = $('#password').val()
  var email = $('#email').val()
  var body = {
    username: username,
    password: password,
    email: email
  }
  $.ajax({
    url: '/adduser',
    method: 'POST',
    contentType: "application/json",
    data: JSON.stringify(body),
    success: success,
    error: error
  })
}

function redirectToVerify(data, status){
  window.location = "/ui/verify"
}

function clearFieldsAndDisplayError(xhr, status, error){
  $("#registerForm")[0].reset()
  $("#message").html(JSON.parse(xhr.responseText).error)
  $("#message").prop('hidden', false)
}
