

$(document).ready(function(){
  $("#register").on('click', function(){ verify(redirectToMain, clearFieldsAndDisplayError); } )
})

function verify(success, error){
  var key = $('#key').val()
  var email = $('#email').val()
  var body = {
    key: key,
    email: email
  }
  $.ajax({
    url: '/verify',
    method: 'POST',
    contentType: "application/json",
    data: JSON.stringify(body),
    success: success,
    error: error
  })
}

function redirectToMain(data, status){
  window.location = "/"
}

function clearFieldsAndDisplayError(xhr, status, error){
  $("#verifyForm")[0].reset()
  $("#message").html(JSON.parse(xhr.responseText).error)
  $("#message").prop('hidden', false)
}
