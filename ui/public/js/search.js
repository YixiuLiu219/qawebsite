

$(document).ready(function(){
  $("#searchBtn").on('click', search(populateSearchResult))
})

function search(callback){
  return function(){
    var url = "/search"
    var text = $("#searchText").val()
    var accepted = $("#acceptedBoolean").prop('checked')
    var media = $("#mediaBoolean").prop('checked')
    var limit = parseInt($("#limitDropdown").val())
    var sort_by = $('#sortByDropdown').val()
    var tags = $('#tagsText').val().trim().split(/[ ,]+/)
    if(tags.length==1 && tags[0]==''){  // [""]
      tags = []
    }
    var timestamp = $("#timestampSelector").val()=="" ? undefined: new Date($("#timestampSelector").val()).getTime()/1000

    var body = {
      q: text,
      timestamp : timestamp,
      accepted : accepted,
      tags: tags,
      has_media: media,
      limit: limit,
      sort_by : sort_by
    }
    console.log("HI")
    $.ajax({
      url: url,
      method: 'POST',
      contentType: "application/json",
      data: JSON.stringify(body),
      success: callback
    })
  }
}
var a = 0;
function populateSearchResult(searchResult){
  a = searchResult
  var questionJSONList = searchResult['questions']
  $("#pageContent").html("")
  for(var key in questionJSONList){
    var question = questionJSONList[key]
    $("#pageContent").append(
      "<div class=\'card\'>"+
        "<div class=\'card-body\'>"+
          "<h5 class=\'card-title'> <a href='/ui/questions/"+question.id+"'>"+question.title+"</a></h5>"+
          "<div class=\'card-text\'>"+question.body+"</div>"+
          "<div><div>User:"+question.user.username+"</div>"+
          "<div>Rep:"+question.user.reputation+"</div></div>"+
          "</div>"+
      "</div>"
    )
  }
}
