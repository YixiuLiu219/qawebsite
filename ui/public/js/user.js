
var userhistory = {
  limit: 10,
  questions : [],
  answers : [],
  questionPointer: 0,
  answerPointer: 0
}

$(document).ready(function(){
  var profileUsername = $('body').attr('data-profile-username')
  console.log(profileUsername)
  populateUserData(profileUsername)
  populateQuestions(profileUsername)
  populateAnswers(profileUsername)
})


function populateUserData(username){
  $.ajax({
    url: '/user/'+username,
    method: 'GET',
    contentType: "application/json",
    success: function(data){
      var user = data.user
      $('#username').html(username)
      $('#email').html(user.email)
      $('#reputation').html(user.reputation)
    }
  })
}

function populateQuestions(username){
  $.ajax({
    url: '/user/'+username+'/questions',
    method: 'GET',
    contentType: "application/json",
    success: function(data){
      var questions = data.questions
      for(var i in questions){
        var item = '<li> <a href=/ui/questions/'+questions[i]+'>'+questions[i]+'</a> </li>'
        userhistory.questions.push(item)
        if(userhistory.questionPointer < userhistory.limit){
          $('#history_q').append(item)
          userhistory.questionPointer++
        }
      }
    }
  })
}

function populateAnswers(username){
  $.ajax({
    url: '/user/'+username+'/answers',
    method: 'GET',
    contentType: "application/json",

    success: function(data){
      var answers = data.answers

      for(var i in answers){
        var item = '<li>'+answers[i]+'</li>'
        userhistory.answers.push(item)
        if(userhistory.answerPointer < userhistory.limit){
          $('#history_a').append(item)
          userhistory.answerPointer++
        }
      }
    }
  })
}
