﻿
$(document).ready(function(){
  var questionID = $('body').attr('data-question-id')
  var currentUser = $('body').attr('data-session-username')
  populateQuestionData(questionID, currentUser, function(userIsAkser, accepted){ populateAnswerData(questionID, userIsAkser, accepted) })
  $('#answer_add').on('click', function(){ answerAdd(questionID) })
  addFileUploadEvent($('#uploadBtn'), $('#content'), addMediaToList)
})

function addTagSpace(){
  var clone = $(document.querySelector('#tags_editor_template').content.cloneNode(true))
  $('#tags_editor_list').append(clone)
}

function addMediaToList(id){
  var clone = $(document.querySelector('#media_editor_template').content.cloneNode(true))
  var item = clone.find('.media_editor_item')
  item.prop('value', id)
  item.prop('readonly', true)
  $('#media_editor_list').append(clone)
  $('#fileUploadModal').modal('hide')
}

function addFileUploadEvent(triggerElement, formFileElement, callback){
  triggerElement.on('click', function(){
    console.log('hello')
    var formData = new FormData()
    formData.append('content', formFileElement[0].files[0])

    $.ajax({
      url: '/addmedia',
      method: 'POST',
      contentType: false,
      processData: false,
      data: formData,
      crossDomain: true,
      success: function(data){
        callback(data.id)
      },
      error: function(xhr, status, error){
        console.log(error)
      }
    })
  })
}

function itemify(text){
  return '<li>' +text+ '</li>'
}

function populateQuestionData(questionID, currentUser, callback){
  $.ajax({
    url: '/questions/'+questionID,
    method: 'GET',
    contentType: "application/json",
    success: function(data){
      var question = data.question
      $('#question_title').append(question.title)
      $('#question_body').append(question.body)
      $('#question_score').append(question.score)

      question.tags.forEach(function(e){
        $('#question_tags').append(itemify(e))
      })
      question.media.forEach(function(e){
        var imgHTML = "<img src='/media/"+e+"'>"
        $('#question_media').append(itemify(e))
        $('#question_media').append(itemify(imgHTML))
      })

      $('#question_user').append(question.user.username)
      $('#question_user_reputation').append(question.user.reputation)

      var userIsAkser = question.user.username == currentUser
      if(userIsAkser){
        var deleteBtn = $('#question_delete')
        deleteBtn.prop("hidden", false)
        addDeleteEvent(deleteBtn, question.id)
      }

      addVoteEvent($('.question .upvote'), true, true, question.id)
      addVoteEvent($('.question .downvote'), false, true, question.id)

      callback(userIsAkser, question.accepted_answer_id)
    },
    error: function (xhr, status, error){
      //window.location = '/'
      $("#question_section").html(JSON.parse(xhr.responseText).error)
      $("#answer_section").html("")
    }
  })
}

function populateAnswerData(questionID, userIsAkser, accepted){
  $.ajax({
    url: '/questions/'+questionID+"/answers",
    method: 'GET',
    contentType: "application/json",
    success: function(data){
      data.answers.forEach(function(e){
        var answer = $(document.querySelector('#answer_template').content.cloneNode(true))
        answer.find(".answer_body")[0].append(e.body)
        answer.find(".answer_score")[0].append(e.score)
        answer.find(".answer_user")[0].append(e.user)
        e.media.forEach(function(media){
          answer.find(".answer_media")[0].append(media)
        })

        if(e.is_accepted){
          answer.find('.answer_accept_mark')[0].append("Accepted Answer ✔")
        }
        else{
          addAcceptEvent(answer.find('.answer_accept'), e.id)
        }

        addVoteEvent(answer.find('.upvote'), true, false, e.id)
        addVoteEvent(answer.find('.downvote'), false, false, e.id)

        $('#answer_list').append(answer)

      })
      if(userIsAkser && !accepted){
        $('.answer_accept').prop('hidden', false)
      }

    }
  })
}

function addAcceptEvent(element, answerID){
///answers/:id/accept
  element.on('click', function(){
    $.ajax({
      url: '/answers/'+answerID+"/accept",
      method: 'POST',
      contentType: "application/json",
      success: function(data){
        window.location.reload(true)
      }
    })
  })
}

function addDeleteEvent(element, questionID){
///answers/:id/accept
  element.on('click', function(){
    $.ajax({
      url: '/questions/'+questionID,
      method: 'DELETE',
      contentType: "application/json",
      success: function(data){
        window.location.reload(true)
      }
    })
  })
}

function addVoteEvent(element, vote, isQuestion, postID){
  element.on('click', function(){
    console.log("HELLO")
    var postType = isQuestion? 'questions' : 'answers'
    $.ajax({
      url: '/'+postType+'/'+postID+"/upvote",
      method: 'POST',
      contentType: "application/json",
      data: JSON.stringify({ upvote: vote }),
      success: function(data){
        window.location.reload(true)
      }
    })
  })
}


function gather(selector){
  var vals = []
  $(selector).each(function(e){
    let value = $(this).val()
    if(value && value.trim()!='')
      vals.push( $(this).val() )
  })
  return vals
}

function answerAdd(questionID){ // gatehr up all data
  var body = {
    body : $('#body_editor').val(),
    media: gather('.media_editor_item')
  }
  $.ajax({
    url: '/questions/'+questionID+'/answers/add',
    method: 'POST',
    contentType: "application/json",
    data: JSON.stringify(body),
    success: function(data){
      window.location.reload(true)
    }
  })
}
