

$(document).ready(function(){
  $("#login").on('click', function(){ login(redirectToMain, clearFieldsAndDisplayError); } )
})

function login(success, error){
  console.log("ASD")
  var username = $('#username').val()
  var password = $('#password').val()
  var body = {
    username: username,
    password: password
  }
  $.ajax({
    url: '/login',
    method: 'POST',
    contentType: "application/json",
    data: JSON.stringify(body),
    success: success,
    error: error
  })
}

function redirectToMain(data, status){
  console.log("SUCCESS")
  window.location = "/"
}

function clearFieldsAndDisplayError(xhr, status, error){
  $("#loginForm")[0].reset()
  $("#message").html(JSON.parse(xhr.responseText).error)
  $("#message").prop('hidden', false)
}
