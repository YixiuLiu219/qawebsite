
$(document).ready(function(){
  $("#question_add").on('click', function(){ questionAdd(redirectToQuestion, clearFieldsAndDisplayError); } )
  $("#tags_add").on('click', addTagSpace )
  addFileUploadEvent($('#uploadBtn'), $('#content'), addMediaToList)
})

function addTagSpace(){
  var clone = $(document.querySelector('#tags_editor_template').content.cloneNode(true))
  $('#tags_editor_list').append(clone)
}

function addMediaToList(id){
  var clone = $(document.querySelector('#media_editor_template').content.cloneNode(true))
  var item = clone.find('.media_editor_item')
  item.prop('value', id)
  item.prop('readonly', true)
  $('#media_editor_list').append(clone)
  $('#fileUploadModal').modal('hide')
}

function addFileUploadEvent(triggerElement, formFileElement, callback){
  triggerElement.on('click', function(){
    console.log('hello')
    var formData = new FormData()
    formData.append('content', formFileElement[0].files[0])

    $.ajax({
      url: '/addmedia',
      method: 'POST',
      contentType: false,
      processData: false,
      data: formData,
      crossDomain: true,
      success: function(data){
        callback(data.id)
      },
      error: function(xhr, status, error){
        console.log(error)
      }
    })
  })
}

function gather(selector){
  var vals = []
  $(selector).each(function(e){
    let value = $(this).val()
    if(value && value.trim()!='')
      vals.push( $(this).val() )
  })
  return vals
}

function questionAdd(success, error){ // gatehr up all data
  var body = {
    title : $('#title_editor').val(),
    body : $('#body_editor').val(),
    tags : gather('.tags'),
    media: gather('.media_editor_item')
  }
  $.ajax({
    url: '/questions/add',
    method: 'POST',
    contentType: "application/json",
    data: JSON.stringify(body),
    success: success,
    error: error
  })
}

function redirectToQuestion(data, status){
  window.location = "/ui/questions/"+data.id
}

function clearFieldsAndDisplayError(xhr, status, error){
  $("#message").html(JSON.parse(xhr.responseText).error)
  $("#message").prop('hidden', false)
}
